#! /bin/bash
set -e

/bin/bash /var/www/pastell/docker/docker-pastell-init > /data/config/DockerSettings.php

# Certbot
if [ "$LETSENCRYPT_DOMAIN" ] ; then
    if [ -f "/data/certificate/fullchain.pem" ] ; then
            certbot  --logs-dir /data/log/ --work-dir /data/run --http-01-port 80 renew
    else
        certbot --http-01-port 80 --logs-dir /data/log/ --work-dir /data/run --standalone --noninteractive --agree-to --preferred-challenges http -d ${LETSENCRYPT_DOMAIN} -m ${LETSENCRYPT_EMAIL} certonly
        ln -s /etc/letsencrypt/live/${LETSENCRYPT_DOMAIN}/fullchain.pem /data/certificate/
        ln -s /etc/letsencrypt/live/${LETSENCRYPT_DOMAIN}/privkey.pem /data/certificate/
    fi
fi

if [ "$LETSENCRYPT_MAILSEC_DOMAIN" ] ; then
    if [ -f "/data/certificate/mailsec_fullchain.pem" ] ; then
            certbot  --logs-dir /data/log/ --work-dir /data/run --http-01-port 80 renew
    else
        certbot --http-01-port 80 --logs-dir /data/log/ --work-dir /data/run --standalone --noninteractive --agree-to --preferred-challenges http -d ${LETSENCRYPT_MAILSEC_DOMAIN} -m ${LETSENCRYPT_EMAIL} certonly
        ln -s /etc/letsencrypt/live/${LETSENCRYPT_MAILSEC_DOMAIN}/mailsec_fullchain.pem /data/certificate/
        ln -s /etc/letsencrypt/live/${LETSENCRYPT_MAILSEC_DOMAIN}/mailsec_privkey.pem /data/certificate/
    fi
fi

/bin/bash /var/www/pastell/docker/wait-for-cacert.sh

if [ -z "$DONT_RETRIEVE_VALIDCA" ] ; then
  if [ ! -d /data/certificate/validca ] ; then
      echo "Récupération des CRL"
      curl -s https://validca.libriciel.fr/retrieve-validca.sh | bash -s /data/certificate
  fi
fi

if [ -z "$DONT_INIT_DATABASE" ] ; then
  php /var/www/pastell/docker/init-docker.php
  /var/www/pastell/bin/console app:bootstrap -vv
fi

export PASTELL_HOST=`echo $PASTELL_SITE_BASE | awk -F[/:] '{print $4}'`
export WEBSEC_HOST=`echo $WEBSEC_BASE | awk -F[/:] '{print $4}'`
exec "$@"
