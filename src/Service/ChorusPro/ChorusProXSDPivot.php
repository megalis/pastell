<?php

namespace Pastell\Service\ChorusPro;

class ChorusProXSDPivot
{
    public function getSchemaPath()
    {
        return __DIR__ . "/xsd-pivot/CPPFacturePivot_V2_02.xsd";
    }
}
