<?php

namespace Pastell\Client\IparapheurV5\Model;

class ObjectIdentifier
{
    public string $objectIdentifierType;
    public string $objectIdentifierValue;
}
