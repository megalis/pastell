<?php

declare(strict_types=1);

namespace Pastell\Client\IparapheurV5\Model;

final class SignificantProperties
{
    public SignificantPropertyType $significantPropertiesType;
    public string $significantPropertiesValue;
}
