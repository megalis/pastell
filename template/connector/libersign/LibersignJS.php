<?php

/**
 * @var string $libersign_applet_url
 * @var string $libersign_extension_update_url
 */

?>

<ls-lib-libersign
        update-url='<?php hecho($libersign_extension_update_url); ?>/'
        applet-url='<?php hecho($libersign_applet_url); ?>/'
>
</ls-lib-libersign>
